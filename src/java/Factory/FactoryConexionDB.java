package Factory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Home PC
 */
public class FactoryConexionDB {
        //podemos definir la configuracion para diferentes BD
    public static final int MySQL = 1; //para conectar a MySQL
    public static String[] configMySQL = {"bd_inventario", "root", ""};
    
    public static ConexionDB open(int tipoBD){
        switch(tipoBD){
            case FactoryConexionDB.MySQL:
                return new MySQLConexionFactory(configMySQL);
            default:
                return null;
        }
        
    }
}
